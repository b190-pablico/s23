console.log("Hello World");
// SECTION - Objects
/*
	- objects are data taypes that are used to represent real world objects
	- collection of related data and/or functionalities
	- in Javascript, most features like strings and arrays are considered to be objects

	- their difference in Javascript objects is that the JS object has "key: value"" pair
	- keys are also referred to as "properties" while the value is the figure on the right side of the colon
	- objects are commonly initialized or declared using the let + objectName and this object value will start with curly brace ({}) that is also called "Object Literals"
	- SYNTAX (using initializers):
		let/const objectName = {
				keyA: valueA,
				keyB: valueB
		}

*/
// creating objects using initializers
let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers");
console.log(cellphone);
console.log(typeof cellphone);

/*
	Miniactivity
		create an object called car
			name/model
			yearReleased

		send the output in our google chat
*/

/*let car = {
	name: "Honda Jazz",
	yearReleased: 2021
}
console.log("Result from creating objects using initializers");
console.log(car);
console.log(typeof car);*/


// creating objects using constructor function
/*
	- creates a reusable function to create/construct several objects that have the same data structure
	- this is useful for creating multiple instances/copies of an object
	- instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it.
	- SYNTAX: 
		function ObjectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB
		}
*/
/*
	this keyword allows to assign a new property for the object by associating them with values received from a contructor function's parameters
*/
function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}
// this is a unique instance/copy of the Laptop object
/*
	- "new" keyword creates an instance of an object
	- objects and instances are often interchanged because object literals and instances are distinct/unique
*/
// let laptop = Laptop("Dell", 2012); - not using "new" keyword would return undefined if we try to log the instance in the console since we don't have return statement in the Laptop function
let laptop = new Laptop("Dell", 2012);
console.log("Result from creating objects using initializers");
console.log(laptop);
console.log(typeof laptop);

// creates a new Laptop object
let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using constructor function");
console.log(laptop2);
console.log(typeof laptop2);

// create empty objects
let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using constructor function");
console.log(myComputer);
console.log(typeof myComputer);

// Accessing objects
/*
	in arrays, we can access the elements inside through the use of square notation: arrayName[index]
	in javascript it's a bit different since we have to use dot notation to access the property/key
		SYNTAX: objectName.key
*/

// using dot notation
console.log("Result from dot notation: " + laptop.name);

console.log("Result from dot notation: " + laptop2.manufacturedDate);

// using square brackets
// console.log("Result from square brackets notation: " + laptop[name]); - returns undefined since there is no element inside the laptop that can be accessed using square bracket notation

// array of objects
/*
	- accessing arrays would the priority since that is the first data type that we have to access
	- since we use [] to access we will use laptops[1] to access the laptop2 element inside the laptops array
	- since the laptop2 is an object, access its properties would require us to use dot notation
	- meaning we have to use laptop2.name to get the value for the "name" property
		hence, we have this SYNTAX:
			arrayName[index].property
*/
let laptops = [ laptop, laptop2 ];
console.log(laptops[1].name);
// accessing laptops array - laptop - manufacturedDate
console.log(laptops[0].manufacturedDate);
// using string inside the second square bracket would let us access the property inside the object. this may lead to confusiona s to what data type is being accessed
console.log(laptops[0] ["manufacturedDate"]);

// SECTION - initializing, adding, deleting and reassigning object properties
/*
	 - like any other variables in javescript, objects may have their their properties initialized or added after the object was created/declared
	- this is useful for times when an object's properties are undetermined at the time of creating them 
*/

let car = {};
console.log(car);
// adding properties
car.name = "Honda Vios";
car.manufacturedDate = 2022;
console.log(car);
/*
	- while using square brackets will give the same feature as using dot notation, it might lean us to create unconventional naming for the properties which might lead future confusions when we try to access them
*/
car["manufactured date"] = 2019;
console.log(car);

// deleting of properties
delete car["manufactured date"];
// delete car.manufacturedDate; - can also be used with dot notation
console.log(car);

// reassigning of properties
/*
	change the name of the car into "Dodge Charger R/T"
*/
car.name = "Dodge Charger R/T";
console.log(car);


// Object Methods
/*
	- an object method is a function that is set by the dev to be the value of one of the properties
	- they are also functions and one of the  differences they have us that methods are functions related to a specific object
	- these are useful for creating object-specific functions which are used to perform tasks on them
	- similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how they should do it.
*/
/*
when we try to outsource a function to be stored inside a property, it would return an error since object methods have to be defined inside an object
function personTalk(){
		console.log("Hello! My name is " + this.name)
	}
*/
let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name)
	}
}

console.log(person);
console.log("Result from object methods:");
person.talk();
/*
	miniactivity
		try to create a walk object method inside the person object
			when we call the method, it would log in the console "<name> walked 25 steps"
*/
// adding a method - use = instead of : when trying to add another property into the object
person.walk = function() {
		// using template literals backticks + ${} + string data type
        console.log(`${this.name} walked 25 steps`);
}
person.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	// nested object
	address: {
		city: "Austin",
		state: "Texas"
	},
	// nested array
	emails: [ 'joe@mail.com', "johnHandsome@mail.com" ],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}
friend.introduce();
console.log(friend);

// real-world application of objects

/*
	Scenario
		- we would like to create a game that would have several pokemon interact with each other
		- every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log( "This Pokemon tackled targetPokemon" );
		console.log( "targetPokemon's health is now reduced to _targetPokemonHealth_" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon);

// using constructor function
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.atttack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	};
	this.faint = function(target) {
		console.log(target.name + " fainted.");
	}
}
// creating new pokemon
let pikachu = new Pokemon ("Pikachu", 16);
let rattata = new Pokemon ("Rattata", 8);

// using the tackle method from pikachu with rattata as its target
pikachu.tackle(rattata);