let trainer = {
	name: "Ash Ketchum",
	age: 12,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log(this.pokemon + "! I choose you!")
	}
}
console.log(trainer)

// dot notation
console.log("Result of dot notation");
console.log(trainer.name);
// square notation
let myPokemon = {};
myPokemon.pokemon = ["Pikachu", "Magikarp", "Ditto", "Mimikyu"]
console.log("Result of square notation");
console.log(myPokemon)
// talk method
console.log("Result of talk method");
trainer.talk();
// Pokemon stats
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.atttack = 1.5 * level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name +" health is now reduced to " + target.health);
		console.log()
	};
	this.faint = function() {
		console.log(this.name + " fainted.");
	}
}
// new pokemons
let pikachu = new Pokemon('Pikachu', 25);
console.log(pikachu);
let magikarp = new Pokemon('Magikarp', 27);
console.log(magikarp);
let ditto = new Pokemon('Ditto', 18);
console.log(ditto);
let mimikyu = new Pokemon('Mimikyu', 24);
console.log(mimikyu);
// Pokemon Battle
ditto.tackle(magikarp);
console.log(magikarp)